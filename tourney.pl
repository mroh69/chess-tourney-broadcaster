#!/usr/bin/perl

use strict;
use warnings FATAL => 'all';

my $rounds = 2;
my $pgn = "booktest.pgn";
my $books_dir = 'books';
my $each = "tc=180+2";
my $engine_conf = "Shredder13";
my $hash = "option.Hash=2048 option.Threads=6";
my $debug = "-debug";
my $draw = ""; #"-draw movenumber=20 movecount=10 score=0.0";

my $gauntlet = ""; #"-tournament gauntlet -engine conf=Shredder13 name=mike book=$books_dir/mike.bin";

# name -> bookfile mapping
my $config = {
    $engine_conf => '',
    'mike' => 'mike.bin',
    'cerebellum' => 'cerebellum.bin',
        'ranomi' => 'Ranomi2.bin',
        'rpc-needle' => 'rpc-needle.bin',
        'myfriends' => 'myfriends.bin',
        'luclis' => 'luclis.bin',
    'stormworm' => 'StormWorm.bin',
    #        'performance' => 'Performance.bin',
	#        'komodo' => 'komodo.bin',
	#        'perfect' => 'Perfect2017.bin',
	#        'rodent' => 'rodent.bin',
	#    'gm2001' => 'gm2001.bin',
	#        'prodeo' => 'ProDeo.bin',
};

my $cmd="./cutechess-cli -tb TB -tbpieces 6 $debug -pgnout $pgn -concurrency 1 -rounds $rounds $draw $gauntlet";

while(my($k,$v) = each %{$config}) {
    $cmd = "$cmd -engine conf=$engine_conf name=$k ";
    if(!$v eq '') {
        $cmd = "$cmd book=$books_dir/$v ";
    }
}

#print "$cmd -each $each";

exec "$cmd -each $each $hash";
