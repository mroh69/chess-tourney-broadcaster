package tourney

import java.io.InputStream
import java.util.*
import java.util.concurrent.Executors
import java.util.concurrent.ScheduledExecutorService
import java.util.concurrent.TimeUnit
import javax.servlet.ServletContextEvent
import javax.servlet.ServletContextListener
import javax.servlet.annotation.WebListener
import javax.websocket.*
import javax.websocket.server.ServerEndpoint

object TourneyManager {
    val ecoCodes = mutableMapOf<String, Pair<String, String>>()
    val tourneys = mutableMapOf("" to Tourney(""))
    fun tourney(name:String) = if (tourneys.contains(name)) tourneys[name]!! else tourneys[""]!!

    @ServerEndpoint("/cutechess/{user}")
    open class CutechessEndpoint {
        @OnOpen
        fun onOpen(session: Session) {
            val userSubPart = if (session.pathParameters["user"] == null) "" else session.pathParameters["user"]!!
            if (tourneys.containsKey(userSubPart)) {
                session.close() // TODO reason
            } else {
                print("connecting tourney: $userSubPart")
                tourneys[userSubPart] = Tourney(userSubPart).apply {
                    reset()
                }
            }
        }

        @OnMessage
        fun receive(msg: String, session: Session) {
            val userSubPart = if (session.pathParameters["user"] == null) "" else session.pathParameters["user"]!!
            tourneys[userSubPart]?.receive(msg)
            if (tourneys.size==1)  {   // send it to default view also
                tourneys[""]?.receive(msg)
            }
        }
    }
}

@WebListener
class MyContextListener : ServletContextListener {
    private lateinit var scheduler: ScheduledExecutorService

    private fun readEcoFile(stream: InputStream?) {
        stream?.use {
            stream.bufferedReader().readLines().forEach {
                val v = it.split('\t')
                TourneyManager.ecoCodes[v[0]] = Pair(v[1], v[2])
            }
        }
    }

    override fun contextInitialized(p0: ServletContextEvent?) {
        readEcoFile(p0?.servletContext?.getResourceAsStream("/WEB-INF/classes/tourney/eco.epd"))

        scheduler = Executors.newSingleThreadScheduledExecutor()
        scheduler.scheduleAtFixedRate({
            val now = Date().time
            val iter = TourneyManager.tourneys.iterator()
            while (iter.hasNext()) {
                val it = iter.next()
                if (it.key.isNotEmpty()) {   // not the default one
                    if (it.value.listeners.isEmpty()) {   // there is someone watching
                        if (TimeUnit.MILLISECONDS.toHours( now - it.value.lastReceivedTS) > 1) {
                            print("removing tourney ${it.key}")
                            iter.remove()
                        }
                    }
                }
            }
        }, 0, 60, TimeUnit.MINUTES)
    }

    override fun contextDestroyed(p0: ServletContextEvent?) {
        scheduler.shutdownNow()
    }
}
