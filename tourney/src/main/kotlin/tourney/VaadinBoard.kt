package tourney

import com.vaadin.annotations.JavaScript
import com.vaadin.annotations.StyleSheet
import com.vaadin.server.VaadinSession
import com.vaadin.shared.communication.ClientRpc
import com.vaadin.shared.communication.ServerRpc
import com.vaadin.shared.ui.JavaScriptComponentState
import com.vaadin.ui.AbstractJavaScriptComponent

// js to server (isnt used)
interface BoardServerRpc : ServerRpc {
    // fen is the pos after the move
    fun onBoardChange(move:String, fen:String)
}

// server to js
interface BoardClientRpc : ClientRpc {
    fun setPosition(fen:String)
    fun makeMove(move:String)
    fun flip()
    fun getFen():String
    fun getPgn():String
    fun getTurn():Boolean
    fun undo()
    fun reset()
}

val BOARD_THEMES = arrayOf("wikipedia", "alpha", "uscf")

// this is for transporting the theme name
class SharedState : JavaScriptComponentState() {
    var themeName = BOARD_THEMES[0]
    var showCoordinates = false
}

@StyleSheet("chessboard-0.3.0.css")
@JavaScript("jquery-3.2.1.min.js", "chessboard-0.3.0.min.js", "chess.js", "vaadinboard_connector.js")
class VaadinBoard() : AbstractJavaScriptComponent(), BoardClientRpc, BoardServerRpc {
    constructor(options:Map<String, String>) : this() { this.options = options }
    private var options:Map<String, String>? = null

    private val boardListener = mutableListOf<BoardServerRpc>()
    var isFlipped = false

    fun addBoardListener(listener:BoardServerRpc) { boardListener.add(listener)}
    fun removeBoardListener(listener:BoardServerRpc) { boardListener.remove(listener) }

    // multiplex board change events
    override fun onBoardChange(move: String, fen:String) {
        for(listener in boardListener)
            listener.onBoardChange(move, fen)
    }

    init {
        val pieces = arrayOf("bB", "bK", "bN", "bP", "bQ", "bR", "wB", "wK", "wN", "wP", "wQ", "wR")

        for(theme in BOARD_THEMES) {
            for (piece in pieces) {
                VaadinSession.getCurrent().communicationManager.registerDependency("chesspieces/$theme/$piece.png", VaadinBoard::class.java)
            }
        }
        registerRpc(this)
    }

    override fun setPosition(fen: String) {
        getRpcProxy(BoardClientRpc::class.java).setPosition(fen)
    }

    override fun makeMove(move: String) {
        getRpcProxy(BoardClientRpc::class.java).makeMove(move)
    }

    override fun flip() {
        getRpcProxy(BoardClientRpc::class.java).flip()
        isFlipped = !isFlipped
    }

    override fun getFen(): String {
        return getRpcProxy(BoardClientRpc::class.java).getFen()
    }

    override fun getPgn(): String {
        return getRpcProxy(BoardClientRpc::class.java).getPgn()
    }

    override fun getTurn(): Boolean {
        return getRpcProxy(BoardClientRpc::class.java).getTurn()
    }

    override fun undo() {
        getRpcProxy(BoardClientRpc::class.java).undo()
    }

    override fun getState(): SharedState {
        val state = super.getState() as SharedState
        state.themeName = if(options!=null) options!!.getOrDefault(BOARD_THEME, BOARD_THEMES[0]) else BOARD_THEMES[0]
        state.showCoordinates = if(options!=null) options!!.getOrDefault(BOARD_COORDINATES, "false").toBoolean() else false

        return state
    }

    override fun reset() {
        getRpcProxy(BoardClientRpc::class.java).reset()
    }
}
