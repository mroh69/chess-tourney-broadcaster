package tourney

import com.github.vok.karibudsl.*
import com.vaadin.event.ShortcutAction
import com.vaadin.server.Page
import com.vaadin.ui.*

const val BOARD_WIDTH = "tourney.board.width"
const val BOARD_THEME = "tourney.board.theme"
const val BOARD_COORDINATES = "tourney.board.coordinates"
const val VARIANT_WIDTH = "tourney.variant.width"
const val ENGINES_WIDTH = "tourney.engines.width"
const val MOVES_WIDTH = "tourney.moves.width"
const val MOVES_HEIGHT = "tourney.moves.height"
const val USEFIGURINE = "font.figurine"

const val COOKIE_AGE = "max-age=2592000"    // 30 days

class OptionsWindow(val options:Map<String, String>) : Window() {
    init {
        val f_layout =
        formLayout {
            val boardWidth = textField("Board Width") {
                value = options.getOrDefault(BOARD_WIDTH, "420px")
                description = "Width of the Board"
            }
            val variantWidth = textField("Variant Board Width") {
                value = options.getOrDefault(VARIANT_WIDTH, "240px")
                description = "Width of the VariantBoard"
            }
            val enginesWidth = textField("Engines Width") {
                value = options.getOrDefault(ENGINES_WIDTH, "540px")
                description = "Width of the Engines Tab"
            }
            val movesWidth = textField("Moves Width") {
                value = options.getOrDefault(MOVES_WIDTH, "310px")
                description = "Width of the Moves Grid"
            }
            val movesHeight = textField("Moves Height") {
                value = options.getOrDefault(MOVES_HEIGHT, "10.0")
                description = "Height of the Moves Grid in Rows (Double!)"
            }
            val themeSelect = nativeSelect<String>("Pieces") {
                description = "which Piece Set to use"
                setItems(BOARD_THEMES.asList())
                setSelectedItem(options.getOrDefault(BOARD_THEME, BOARD_THEMES[0]))
            }
            val showCoordinates = checkBox("Coordinates", options.getOrDefault(BOARD_COORDINATES, "false").toBoolean()) {
                description = "if the board shows coordinates"
            }
            val useFigurine = checkBox("use informator font", options.getOrDefault(USEFIGURINE, "true").toBoolean()) {
                description = "if you would like figurine font for moves"
            }
            horizontalLayout{
                button("Cancel", { close() }) {
                    setClickShortcut(ShortcutAction.KeyCode.ESCAPE)
                }
                button("Save", {
                    // because of potential push, do it with javascript. path doesnt work?!
                    JavaScript.getCurrent().execute("document.cookie='$BOARD_WIDTH=${boardWidth.value};$COOKIE_AGE';" +
                            "document.cookie='$VARIANT_WIDTH=${variantWidth.value};$COOKIE_AGE';" +
                            "document.cookie='$ENGINES_WIDTH=${enginesWidth.value};$COOKIE_AGE';" +
                            "document.cookie='$MOVES_WIDTH=${movesWidth.value};$COOKIE_AGE';" +
                            "document.cookie='$MOVES_HEIGHT=${movesHeight.value};$COOKIE_AGE';" +
                            "document.cookie='$BOARD_THEME=${themeSelect.value};$COOKIE_AGE';" +
                            "document.cookie='$USEFIGURINE=${useFigurine.value};$COOKIE_AGE';" +
                            "document.cookie='$BOARD_COORDINATES=${showCoordinates.value};$COOKIE_AGE';"
                    )
                    close()
                    Page.getCurrent().reload()
                }) {
                    setClickShortcut(ShortcutAction.KeyCode.ENTER)
                }
            }
        }

        isModal = true
        setWidth("30em")
        center()
        content = f_layout
    }

}