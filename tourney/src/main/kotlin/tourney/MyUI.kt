package tourney

import javax.servlet.annotation.WebServlet
import java.text.DecimalFormat
import java.text.SimpleDateFormat
import java.util.*

import com.vaadin.ui.*
import com.vaadin.annotations.VaadinServletConfiguration
import com.vaadin.annotations.Push
import com.vaadin.contextmenu.ContextMenu
import com.vaadin.server.VaadinRequest
import com.vaadin.server.VaadinServlet
import com.vaadin.shared.ui.ContentMode
import com.github.vok.karibudsl.*
import com.vaadin.annotations.StyleSheet
import com.vaadin.annotations.Title
import com.vaadin.icons.VaadinIcons
import com.vaadin.server.VaadinService
import com.vaadin.server.VaadinSession
import com.vaadin.ui.renderers.HtmlRenderer

import freeAquarium.chess.UciInfoState
import freeAquarium.chess.toHumanEval
import raptor.chess.*

data class MoveListGrid(val moveNumber: Int = 1) {
    var whiteMove = ""
    var blackMove = ""
    var whiteUciInfo: UciInfoState? = null
    var blackUciInfo: UciInfoState? = null
}

@Push
@Title("Tourney Broadcast")
@StyleSheet("freeaquarium.css")
class MyUI : UI(), CuteListener {
    val options = mutableMapOf<String, String>()

    val board = VaadinBoard(options)
    private val moves = Grid<MoveListGrid>()
    private val moveList = mutableListOf<MoveListGrid>()
    private val whiteClock = Label()
    private val blackClock = Label()
    private val openingName = Label()
    private val outOfBookWhite = Label()
    private val outOfBookBlack = Label()
    private val whiteUci = UciInfoOutput(this)
    private val blackUci = UciInfoOutput(this)
    private var subPart:String = ""
    fun myTourney() = TourneyManager.tourney(subPart)

    override fun init(request: VaadinRequest?) {
        VaadinService.getCurrentRequest().cookies?.forEach {
            if (it.name!=null && it.value!=null)
                options[it.name] = it.value
        }

        VaadinSession.getCurrent().communicationManager.registerDependency("ChessInformantReader.woff", MyUI::class.java)

        subPart = request?.pathInfo!!.filter { it!='/' }

        val ml =
        verticalLayout {
            menuBar {
                item("Info")  {
                    item("Results", {
                        if (myTourney().games.isNotEmpty()) {
                            val w = Window("Results")
                            val l = Label(myTourney().gamesAsHTML(), ContentMode.HTML).apply { setWidth("100%") }
                            w.content = VerticalLayout(l).apply { setExpandRatio(l, 1.0F) }
                            w.center()
                            addWindow(w)
                        }
                    }).icon = VaadinIcons.INFO
                    item("Crosstable", {
                        if (myTourney().games.isNotEmpty()) {
                            val w = Window("Crosstable").apply {
                                content = Label(myTourney().crossTable(), ContentMode.HTML).apply { setWidth("100%") }
                                center()
                            }
                            addWindow(w)
                        }
                    }).icon = VaadinIcons.INFO
                    addSeparator()
                    item("Current PGN", { pgnWindow() })
                    item("Current FEN", { fenWindow() })
                    addSeparator()
                    item("Flip Board", { board.flip() }).icon = VaadinIcons.FLIP_V
                    addSeparator()
                    item("Options", { addWindow(OptionsWindow(options)) }).icon = VaadinIcons.OPTIONS
                }
                setSizeFull()
            }
        }

        val hl = HorizontalLayout()

        with(board) {   // doesnt work inside dsl
            setWidth(options.getOrDefault(BOARD_WIDTH, "420px"))
        }
        with(ContextMenu(board, true)) {
            addItem("flip", { board.flip() }).icon = VaadinIcons.FLIP_V
            addItem("show fen string", { fenWindow() }).icon = VaadinIcons.INFO
            addItem("show pgn string", { pgnWindow() }).icon = VaadinIcons.INFO
        }

        with(moves) {
            heightByRows = options.getOrDefault(MOVES_HEIGHT, "10.0").toDoubleOrNull() ?: 10.0
            setWidth(options.getOrDefault(MOVES_WIDTH, "310px"))
            setSelectionMode(Grid.SelectionMode.NONE)
            with(addColumn({ it.moveNumber })) {
                id = "nr"
                caption = "nr"
                setSortable(false)
            }

            val useFigurine = options.getOrDefault(USEFIGURINE, "true").toBoolean()
            with( if(useFigurine) addColumn({ sanToFigurine(it.whiteMove, "informant-grid-column") }, HtmlRenderer()) else addColumn { it.whiteMove }) {
                id = "whiteMove"
                caption = myTourney().whitePlayer
                isSortable = false
                setDescriptionGenerator {
                    if (it?.whiteUciInfo == null)
                        "bookmove"
                    else
                        toHumanEval(it.whiteUciInfo?.score, it.whiteUciInfo?.depth?.toLong()) + " " + fmt_d.format(it.whiteUciInfo!!.time/1000.0) + 's'
                }
            }
            with( if(useFigurine) addColumn({ sanToFigurine(it.blackMove, "informant-grid-column") }, HtmlRenderer()) else addColumn { it.blackMove }) {
                id = "blackMove"
                caption = myTourney().blackPlayer
                isSortable = false
                setDescriptionGenerator {
                    if (it?.blackUciInfo == null)
                        "bookmove"
                    else
                        toHumanEval(it.blackUciInfo?.score, it.blackUciInfo?.depth?.toLong()) + " " + fmt_d.format(it.blackUciInfo!!.time/1000.0) + 's'
                }
            }
        }
        hl.addComponent(board)
        hl.addComponent(moves)

        whiteClock.caption = myTourney().whitePlayer
        blackClock.caption = myTourney().blackPlayer
        whiteClock.value = time_fmt.format(Date(myTourney().whiteClock.toLong()))
        blackClock.value = time_fmt.format(Date(myTourney().blackClock.toLong()))
        openingName.caption = "Opening"
        updateEco()
        hl.addComponent(VerticalLayout(whiteClock, blackClock, openingName, outOfBookWhite, outOfBookBlack))

        ml.addComponent(hl)
        whiteUci.caption = myTourney().whitePlayer
        blackUci.caption = myTourney().blackPlayer
        ml.addComponent(HorizontalLayout(whiteUci, blackUci))

        outOfBookWhite.value = myTourney().firstWhiteUciOOB
        outOfBookBlack.value = myTourney().firstBlackUciOOB
        outOfBookWhite.caption = "${myTourney().whitePlayer} first out of book move"
        outOfBookBlack.caption = "${myTourney().blackPlayer} first out of book move"

        myTourney().listeners.add(this)

//        Page.getCurrent().styles.add(".players-turn {background-color: black;color: white;}")

        content = ml
    }

    private fun pgnWindow() {
        val w = Window("PGN").apply {
            content = Label(myTourney().game.toPgn(), ContentMode.PREFORMATTED)
            center()
        }
        addWindow(w)
    }

    private fun fenWindow() {
        val w = Window("Position").apply {
            content = Label(myTourney().game.toFen())
            center()
        }
        addWindow(w)
    }

    override fun attach() {
        super.attach()

        board.setPosition(myTourney().game.toFen())

        myTourney().getCurrentMoves().split(' ').forEach { move ->
            if (move.isNotBlank()) {
                appendToMoveList(move)
            }
        }

        moves.setItems(moveList)
        moves.scrollToEnd()
        updatePlayersTurn()
        updateEco()
    }

    override fun close() {
        super.close()
        myTourney().listeners.remove(this)
    }

    override fun receiveMove(move: String, uciInfo: UciInfoState?) {
        access {
            board.setPosition(myTourney().game.toFen())
            appendToMoveList(move, uciInfo)
            moves.setItems(moveList)
            moves.scrollToEnd()

            updateEco()
            updatePlayersTurn()
        }
    }

    override fun startNewGame(whiteName: String, blackName: String) {
        access {
            moveList.clear()
            board.reset()
            moves.getColumn("whiteMove").caption = whiteName
            moves.getColumn("blackMove").caption = blackName
            whiteClock.caption = "$whiteName Clock"
            blackClock.caption = "$blackName Clock"
            whiteUci.caption = "<b>$whiteName</b>"
            blackUci.caption = "<b>$blackName</b>"
            moves.setItems(moveList)
            outOfBookWhite.value = ""
            outOfBookBlack.value = ""
            outOfBookWhite.caption = "$whiteName first out of book move"
            outOfBookBlack.caption = "$blackName first out of book move"
            updateEco()
        }
    }

    override fun clockChange(whiteTime: Int, blackTime: Int) {
        access {
            whiteClock.value = time_fmt.format(Date(whiteTime.toLong()))
            blackClock.value = time_fmt.format(Date(blackTime.toLong()))
        }
    }

    override fun bestmove(move: String, ponder: String, isWhite:Boolean) {
        access {
            if (isWhite) {
                whiteUci.ponder.value = ponder
                whiteUci.lastmove.value = move
                outOfBookWhite.value = myTourney().firstWhiteUciOOB
            }
            else {
                blackUci.ponder.value = ponder
                blackUci.lastmove.value = move
                outOfBookBlack.value = myTourney().firstBlackUciOOB
            }
        }
    }

    override fun uciInfo(info: UciInfoState, isWhite: Boolean, convertedPV: String) {
        access {
            with( if(isWhite) whiteUci else blackUci ) {
                if (convertedPV.isNotEmpty()) {
                    if (options.getOrDefault(USEFIGURINE, "true").toBoolean()) {
                        val str = StringBuilder()

                        convertedPV.split(" ").listIterator().forEach {
                            str.append(sanToFigurine(it, "informant-grid-column")).append(' ')
                        }

                        uci.value = str.toString()
                    }
                    else
                        uci.value = convertedPV
                }
                else
                    uci.value = ""

                score.value = toHumanEval(info.score, 0L)
                depth.value = info.depth.toString()
                nps.value = fmt_d.format(info.nps / 1000000.0)
                fill.value = (info.hashfull / 1000.0).toFloat()
                fill.description = fmt_d.format(info.hashfull / 10.0) + '%'
                time.value = fmt_d.format(info.time / 1000.0) + 's'
            }
        }
    }

    private fun appendToMoveList(move:String, uciInfo: UciInfoState? = null) {
        var last = moveList.lastOrNull()
        if (last==null) {
            moveList.add(MoveListGrid())
        }

        last = moveList.last()
        when {
            last.whiteMove.isEmpty() -> {
                last.whiteMove = move
                last.whiteUciInfo = uciInfo
            }
            last.blackMove.isEmpty() -> {
                last.blackMove = move
                last.blackUciInfo = uciInfo
            }
            else -> {
                val new = MoveListGrid(last.moveNumber+1)
                new.whiteMove = move
                last.whiteUciInfo = uciInfo
                moveList.add(new)
            }
        }
    }

    private fun updateEco() {
        openingName.value = myTourney().openingName
    }

    private fun updatePlayersTurn() {
        if (myTourney().game.isWhitesMove) {
            blackClock.icon = null
            whiteClock.icon = VaadinIcons.CLOCK
            with(whiteUci) {
                isEnabled = true
                icon = VaadinIcons.CLOCK
                lastmove.value = ""
                ponder.value = ""
            }
            with(blackUci) {
                icon = null
                isEnabled = false
            }
        }
        else {
            whiteClock.icon = null
            blackClock.icon = VaadinIcons.CLOCK
            with(whiteUci) {
                icon = null
                isEnabled = false
            }
            with(blackUci) {
                isEnabled = true
                icon = VaadinIcons.CLOCK
                lastmove.value = ""
                ponder.value = ""
            }
        }
    }

    companion object {
        private val time_fmt = SimpleDateFormat("mm:ss")
        private val fmt_d = DecimalFormat("##0.0")
    }
}

@WebServlet(urlPatterns = ["/*"], name = "MyUIServlet", asyncSupported = true)
@VaadinServletConfiguration(ui = MyUI::class, productionMode = true)
class MyUIServlet : VaadinServlet()
