package tourney

import freeAquarium.chess.UciInfoState
import freeAquarium.chess.toHumanEval
import raptor.chess.*
import raptor.chess.pgn.PgnHeader
import java.util.*

interface CuteListener {
    fun receiveMove(move:String, uciInfo: UciInfoState?)
    fun startNewGame(whiteName:String, blackName:String)
    fun clockChange(whiteTime:Int, blackTime:Int)  // both in ms
    fun bestmove(move:String, ponder:String, isWhite:Boolean)
    fun uciInfo(info:UciInfoState, isWhite:Boolean, convertedPV:String)
}

private val moves_regex = Regex(" position startpos moves (.*)\$")
private val startGame_regex = Regex("""^Started game \d+ of \d+ \((\S+) vs (\S+)\)$""")
private val finishedGame_regex = Regex("""^Finished game \d+ \((\S+) vs (\S+)\): (.*) \{(.*)}$""")
private val time_regex = Regex(""" go wtime (\d+) btime (\d+)""")
private val bestmove_regex = Regex("""bestmove (\w+) ponder (\w+)$""")
private val cutechess_regex = Regex("""^\d+ [><](\S+)\(\d+\): (.*)$""")

data class Game(val whiteName: String, val blackName: String, val result: String, val reason: String)
class Points {
    var points:Int = 0
    var sb:Double = 0.0
}

class Tourney(val name:String) {
    val listeners = mutableSetOf<CuteListener>()

    var game = GameFactory.createStartingPosition(Variant.classic)!!

    val games = mutableListOf<Game>()
    val players = mutableSetOf<String>()
    private val tourneyStanding = mutableMapOf<String, Points>()

    private fun sortedTourneyStanding(): SortedMap<String, Points> =
            tourneyStanding.toSortedMap(Comparator { o1, o2 ->
                val p1 = tourneyStanding[o1]!!
                val p2 = tourneyStanding[o2]!!
                if (p1.points == p2.points) {
                    if (p1.sb == p2.sb) {
                        o1.compareTo(o2)
                    } else {
                        p2.sb.compareTo(p1.sb)
                    }
                } else {
                    p2.points.compareTo(p1.points)
                }
            })

    fun crossTable(): String {
        val sorted = sortedTourneyStanding().toList()
        val str = StringBuilder("<table border=\"1\"><tbody>")

        str.append("<tr><th>P</th><th>Name</th><th>Points</th><th>SB</th>")
        for (i in 1..sorted.size) {
            str.append("<th>$i</th>")
        }
        str.append("</tr>")

        for ((x, entry) in sorted.withIndex()) {
            val playerName = entry.first
            str.append("<tr><td>${x + 1}.</td><td>$playerName</td><td>${entry.second.points / 2.0}/${countGames(playerName)}</td><td>${entry.second.sb / 2.0}</td>")

            for (i in 0 until sorted.size) {
                str.append("<td>")
                if (playerName == sorted[i].first) {
                    //str.append("#")
                } else {
                    for (game in games) {
                        if (playerName == game.whiteName && sorted[i].first == game.blackName) {
                            str.append(
                                    when (game.result) {
                                        "1-0" -> "1"
                                        "0-1" -> "0"
                                        "1/2-1/2" -> "="
                                        else -> ""
                                    })
                        } else {
                            if (playerName == game.blackName && sorted[i].first == game.whiteName) {
                                str.append(
                                        when (game.result) {
                                            "1-0" -> "0"
                                            "0-1" -> "1"
                                            "1/2-1/2" -> "="
                                            else -> ""
                                        })
                            }
                        }
                    }
                }

                str.append("</td>")
            }
            str.append("</tr>")
        }

        str.append("</tbody></table>")
        return str.toString()
    }

    fun gamesAsHTML(): String {
        val str = StringBuilder("<table><tbody>")

        str.append("<tr><th>Game</th><th>Result</th><th>Reason</th></tr>")
        for (game in games) {
            str.append("<tr><td>${game.whiteName} - ${game.blackName}</td><td>${game.result}</td><td>${game.reason}</td></tr>")
        }

        str.append("</tbody></table>")
        return str.toString()
    }

    private fun countGames(player: String) = games.count { it.whiteName == player || it.blackName == player }

    private var smithMoves = ""
    private var convertedMoveList = ""
    fun getCurrentMoves() = convertedMoveList

    private var whiteName = ""
    val whitePlayer get() = whiteName
    private var blackName = ""
    val blackPlayer get() = blackName
    private var whiteTime = 0
    val whiteClock get() = whiteTime
    private var blackTime = 0
    val blackClock get() = blackTime
    private var lastMove = ""
    private var ponder = ""
    private var uciInfo = UciInfoState()
    private var firstWhiteOOB = ""
    val firstWhiteUciOOB get() = firstWhiteOOB
    private var firstBlackOOB = ""
    val firstBlackUciOOB get() = firstBlackOOB
    private var variationFen = ""
    val fenVariation get() = variationFen
    private var opening = ""
    val openingName get() = opening
    private var whiteDepth = 0
    private var blackDepth = 0

    var lastReceivedTS = 0L

    fun reset() {
        games.clear()
        players.clear()
        tourneyStanding.clear()
        gameReset()
    }

    private fun gameReset() {
        smithMoves = ""
        whiteName = ""
        blackName = ""
        lastMove = ""
        ponder = ""
        uciInfo = UciInfoState()
        firstWhiteOOB = ""
        firstBlackOOB = ""
        whiteDepth = 0
        blackDepth = 0
        game = GameFactory.createStartingPosition(Variant.classic)!!
        variationFen = ""
        opening = ""
    }

    private fun updateTourney(g: Game) {
        if (!tourneyStanding.containsKey(g.whiteName)) {
            tourneyStanding[g.whiteName] = Points()
        }
        if (!tourneyStanding.containsKey(g.blackName)) {
            tourneyStanding[g.blackName] = Points()
        }

        when (g.result) {
            "1-0" -> tourneyStanding[g.whiteName]!!.points += 2
            "1/2-1/2" -> {
                tourneyStanding[g.whiteName]!!.points++
                tourneyStanding[g.blackName]!!.points++
            }
            "0-1" -> tourneyStanding[g.blackName]!!.points += 2
        }

        // sonneborn berger recalc

        for (entry in tourneyStanding) {
            entry.value.sb = 0.0
        }

        for (game in games) {
            when (game.result) {
                "1-0" -> {
                    tourneyStanding[game.whiteName]!!.sb += tourneyStanding[game.blackName]!!.points
                }
                "1/2-1/2" -> {
                    tourneyStanding[game.whiteName]!!.sb += tourneyStanding[game.blackName]!!.points / 2.0
                    tourneyStanding[game.blackName]!!.sb += tourneyStanding[game.whiteName]!!.points / 2.0
                }
                "0-1" -> {
                    tourneyStanding[game.blackName]!!.sb += tourneyStanding[game.whiteName]!!.points
                }
            }
        }
    }

    private fun updateECO() {
        val fen = toShortFen(game.toFen())
        if (TourneyManager.ecoCodes.containsKey(fen)) {
            val theEco = TourneyManager.ecoCodes[fen]!!
            opening = "${theEco.second} (${theEco.first})"
            game.setHeader(PgnHeader.ECO, theEco.first)
            game.setHeader(PgnHeader.Opening, theEco.second)
        }
    }

    fun receive(msg: String) {
        lastReceivedTS = Date().time

        var m = moves_regex.find(msg)
        if (m!=null && m.groups.isNotEmpty()) {
            val theMoves = m.groups[1]!!.value
            if (theMoves.isNotBlank()) {
                if(smithMoves.length > theMoves.length) {
                    println("missed a new game string?")
                }
                else {
                    val newMove = theMoves.substring(smithMoves.length).trim()
                    if (newMove.isNotBlank()) {
                        smithMoves = theMoves
                        fireNewMove(newMove)
                    }
                }
            }
            return
        }

        m = startGame_regex.find(msg)
        if (m!=null && m.groups.isNotEmpty()) {
            gameReset()
            whiteName = m.groups[1]!!.value
            blackName = m.groups[2]!!.value
            players.add(whiteName)
            players.add(blackName)
            fireStartNewGame(whiteName, blackName)
            return
        }

        m = finishedGame_regex.find(msg)
        if (m!=null && m.groups.isNotEmpty()) {
            val g = Game(m.groups[1]!!.value, m.groups[2]!!.value, m.groups[3]!!.value, m.groups[4]!!.value)
            games.add(g)
            updateTourney(g)
            return
        }

        m = time_regex.find(msg)
        if (m!=null && m.groups.isNotEmpty()) {
            whiteTime = m.groups[1]!!.value.toInt()
            blackTime = m.groups[2]!!.value.toInt()
            fireClockChange(whiteTime, blackTime)
            return
        }

        // for the next one, we need to know the cutechess string
        var cutechess_playername = ""
        m = cutechess_regex.find(msg)
        if (m!=null && m.groups.isNotEmpty()) {
            cutechess_playername = m.groups[1]!!.value
        }

        if(cutechess_playername.isNotBlank()) {
            val isWhite = cutechess_playername == whiteName
            val pureUCIString = m!!.groups[2]!!.value

            if (pureUCIString.startsWith("info ")) {
                uciInfo.update(pureUCIString)
                fireInfo(isWhite)
                return
            }

            m = bestmove_regex.find(pureUCIString)
            if (m != null && m.groups.isNotEmpty()) {
                lastMove = m.groups[1]!!.value
                ponder = m.groups[2]!!.value
                fireBestmove(lastMove, ponder, isWhite)
                return
            }
        }
    }

    private fun fireNewMove(move:String) {
        val san = game.moveStringToSan(move, false)
        try { game.makeConvertedMove(move, false) }
        catch(e:IllegalArgumentException) {}

        convertedMoveList = "$convertedMoveList$san "

        updateECO()

        for (listener in listeners) {
            listener.receiveMove(san, if(uciInfo.depth == 0.toShort()) null else uciInfo)
        }
        uciInfo = UciInfoState(!uciInfo.isWhite)
    }

    private fun fireStartNewGame(white:String, black:String) {
        updateECO()

        for (listener in listeners) {
            listener.startNewGame(white, black)
        }
        game.setHeader(PgnHeader.White, white)
        game.setHeader(PgnHeader.Black, black)

        // TODO
        fireInfo(true)  // to reset the uci info in the gui
        fireBestmove("", "", true)
        fireInfo(false)
        fireBestmove("", "", false)
    }

    private fun fireClockChange(white:Int, black:Int) {
        for (listener in listeners) {
            listener.clockChange(white, black)
        }
        uciInfo = UciInfoState(uciInfo.isWhite)
    }

    private fun fireBestmove(move:String, ponder:String, isWhite:Boolean) {
        if (uciInfo.depth > 0) {
            if(isWhite) {
                if(firstWhiteOOB.isEmpty()) {
                    firstWhiteOOB = "${game.fullMoveCount}.$move ${toHumanEval(uciInfo.score, uciInfo.depth.toLong())}"
                }
                whiteDepth += uciInfo.depth
            }
            else {
                if(firstBlackOOB.isEmpty()) {
                    firstBlackOOB = "${game.fullMoveCount}...$move ${toHumanEval(uciInfo.score, uciInfo.depth.toLong())}"
                }
                blackDepth += uciInfo.depth
            }
        }
        for (listener in listeners) {
            listener.bestmove(move, ponder, isWhite)
        }
    }

    private fun fireInfo(isWhite:Boolean) {
        val varGame = game.deepCopy(true)   // lock?
        val str = StringBuilder()

        try {
            uciInfo.pv.split(" ").listIterator().forEach {
                val move = varGame!!.moveStringToSan(it)
                varGame.makeConvertedMove(it, false)
                str.append(move).append(' ')
            }
        }
        catch (e: java.lang.IllegalArgumentException) {}

        variationFen = varGame.toFen()

        for (listener in listeners) {
            listener.uciInfo(uciInfo, isWhite, str.toString())
        }
    }
}
