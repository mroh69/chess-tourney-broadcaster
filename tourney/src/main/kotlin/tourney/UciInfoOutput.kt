package tourney

import com.github.vok.karibudsl.*
import com.vaadin.annotations.StyleSheet
import com.vaadin.shared.ui.ContentMode
import com.vaadin.ui.*

@StyleSheet("freeaquarium.css")
class UciInfoOutput(myUI: MyUI) : Panel() {
    lateinit var nps:Label
    lateinit var fill:ProgressBar
    lateinit var score:Label
    lateinit var depth:Label
    lateinit var uci:Label
    lateinit var time:Label
    lateinit var lastmove:Label
    lateinit var ponder:Label

    private val board = VaadinBoard(myUI.options)
    val popup = PopupView(null, board)

    init {
        isCaptionAsHtml = true
        caption = "<b>Engine1</b>"

        content =
        verticalLayout {
            horizontalLayout {
                lastmove = label {
                    caption = "lastmove"
                }
                ponder = label {
                    caption = "ponder"
                }
            }
            horizontalLayout {
                score = label {
                    setWidth("4em")
                    caption = "score"
                }
                depth = label {
                    setWidth("2em")
                    caption = "d"
                    description = "depth"
                }
                time = label {
                    setWidth("3em")
                    caption = "time"
                }
                nps = label {
                    setWidth("4em")
                    caption = "MNps"
                }
                fill = progressBar {
                    setWidth("2em")
                    caption = "hash"
                }
            }
            uci = label {
                if (myUI.options.getOrDefault(USEFIGURINE, "true").toBoolean()) {
                    contentMode = ContentMode.HTML
                    addStyleName("my-label")
                }
                setHeight("4em")
                setWidth(myUI.options.getOrDefault(ENGINES_WIDTH, "540px"))
                caption = "pv"
                description = "Right click to show variation"
            }
        }.apply { addComponent(popup)  }

        board.setWidth(myUI.options.getOrDefault(VARIANT_WIDTH, "240px"))
        uci.addContextClickListener({
            if (myUI.myTourney().fenVariation.isNotEmpty()) {
                board.setPosition(myUI.myTourney().fenVariation)
                if (myUI.board.isFlipped)
                    board.flip()
                popup.isPopupVisible = true
            }
        })
    }
}