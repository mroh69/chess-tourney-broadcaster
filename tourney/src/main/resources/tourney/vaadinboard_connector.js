window.tourney_VaadinBoard = function() {
    var connector = this;

    // Client rpc callbacks
    this.registerRpc({
        flip: function() { board.flip(); },
        setPosition: function(fen) {
            board.position(fen);
            game.load(fen);
        },
        getFen: function () {
            return game.fen();
        },
        makeMove: function(move) {
            var themove = game.move(move, {sloppy:true});
            board.position(game.fen());
            boardEl.find('.square-55d63').removeClass('highlight1-32417');
            boardEl.find('.square-'+themove.from).addClass('highlight1-32417');
            boardEl.find('.square-'+themove.to).addClass('highlight1-32417');
        },
        getPGN: function() {
            return game.pgn();
        },
        getTurn: function() {
            return game.turn().eq('w')
        },
        undo: function() {
            game.undo();
            board.position(game.fen());
        },
        reset: function() {
            game.reset();
            board.start(false);
        }
    });

    function pieceTheme(piece) {
        var theme = connector.getState().themeName;
        return 'APP/PUBLISHED/chesspieces/' + theme + '/' + piece + '.png';
    }

    var cfg = {
        pieceTheme: pieceTheme,
        position: 'start',
        showNotation: connector.getState().showCoordinates
    };

    var game = new Chess();
    var board = ChessBoard(this.getElement(), cfg);
    var boardEl = $(this.getElement());
};