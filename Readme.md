This is for broadcasting cutechess-cli games, currently only one at a time (concurrent=1, ponder off), as seen [here](https://www.rohleder.de/tourney).

Cloning
=======

Currently, I have some submodules, so please clone with `git clone --recursive`. If you forget that, do a `git submodule --init --recursive` in the checkout dir.


Build
=====

`./gradlew` (If you dont have/need/want docker, `./gradlew war` is enough)
should do the job.
 
   
Run
===

If you have docker running:
`docker run -p 8080:8080 tourney` and browse to http://localhost:8080/tourney should work.

If not: copy the war from tourney/build/libs/tourney.war to your servlet container (tomcat,jetty,undertow, etc).

I played around a bit with [travis ci](https://travis-ci.org/) (very nice service, btw) and the result is [this war](https://dl.bintray.com/mroh69/chess-tourney-broadcaster/0.1/tourney.war) file that gets automatically build. This should also be deployable to you servlet container.

Docker image to pull from docker hub, automatically build from the above:
`docker pull mroh/chess-tourney-broadcaster`


Broadcasting
============

you need [cutechess](https://github.com/cutechess/cutechess), one or more uci chess engines (eg [cfish](https://github.com/syzygy1/Cfish)) and a [tool](https://github.com/mroh69/wsc/tree/uciEngineBridge) I hacked 
to connect the cutechess-cli -debug output to a websocket in the servlet container.   

Lets do it step by step:

Try if `./cutechess-cli -engine cmd=CFish -engine cmd=CFish -each tc=180 -debug` gets any output. You should get lots of uci engine strings.

If not, you might run into the problem that the qt debug level is not "low" enough. (I had this on linux, took me some time to find...). Try this:

vi ~/.config/QtProject/qtlogging.ini and set "*.debug" to true:           
*.debug=true

or just do `export QT_LOGGING_RULES="*.debug=true"`

After that you need the mentioned go tool to pipe the uci strings to the server. With this you are able to run the engines somewhere else than the webserver.

Checkout https://github.com/mroh69/wsc/tree/uciEngineBridge  (the branch is important) and compile it to, lets say "wsc". I also have some precompiled versions [here](https://github.com/mroh69/wsc/releases/tag/v1.0).

If you have that, you can pipe the cutechess output with something like this:
`./cutechess-cli -engine name=CFish1 cmd=cfish -engine name=CFish2 cmd=cfish -each tc=180 proto=uci -debug | ./wsc -u ws://localhost:8080/tourney/cutechess/mytourney`

Thats it, you now should see these engines playing in the browser! ^^


Service
=====

You can broadcast several tourneys at once if you change the url of the entry page and the websocket endpoint:  
http://localhost:8080/tourney/another-tourney becomes ws://localhost:8080/tourney/cutechess/another-tourney for the websocket (-u parameter).  


I run this service at [https://www.rohleder.de/tourney](https://www.rohleder.de/tourney).  
You can broadcast your cutechess tourney by choosing a random name and than use that for the urls:  
https://www.rohleder/tourney/myfirsttourney for the broadcast url and  
ws://www.rohleder.de/tourney/cutechess/myfirsttourney as the websocket endpoint url.

Have fun!

